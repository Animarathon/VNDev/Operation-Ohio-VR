# Animarathon XV: Operation Ohio Virtual Reality

## About the game

This is the official visual novel for [Animarathon XV](https://animarathon.com/).

This is an offshoot of the main repository for Operation Ohio.

This was done to seperate two seperate engines used to make the game.

This repo contains the [A-Frame](https://aframe.io/) based mini section.

## VR Design Document.

Inside the Visual Novel the player clicks on a link that opens their web browser.

The player is placed in a silent theater as found in Bowling Green.

Do not change the setting. I hope to preserve this theater that is about to be demolished. -MPB

The player is told to "Calibrate" their headset by looking up.

The "Calibration" is a total lie.

The player is then bamboozled with a message - "Made you look. :^)"

The player then takes off their headset and curses.

## About Animarathon

Animarathon XV was an anime convention held on March 25th 2017 at the Bowen Thompson Student Union in Bowling Green Ohio.

Anime In Northwest Ohio, AKA ANO/ANWO, is the student organization that runs Animarathon.

Animarathon XV (and by extension this game) was made in cooperation with or sponsorship of

* The Office of Campus Activities

* Event Services

* Kaze No Daichi Taiko

* The Gaming Society

* Japanese Club

* Ziggabyte

* The Wood County Alcohol, Drug Addiction and Mental Health Services Board.

* GeePM

* Matt Visual

* Dirk Manning

* The Coca-Cola Company

Thank you all!
